# Assignment #3

Learn git from: https://git-scm.com/docs/gittutorial

Before beginning with the assignment make sure you registered on http://gitlab.com/

If you have not registered yet, please create an account on http://gitlab.com/


1. Fork this repository on gitlab account: https://gitlab.com/symb-assessment/startbootstrap-bare
2. Add tag to the repository as version *v0.0.1*
3. Create new branch 'media_integration' and checkout it.
4. Create a folder 'assets' and add 2 image file in that:
    1. about_us.png
    2. map.jpg
5. Commit the code
6. Create branch 'portfolio' from master and checkout it
7. Add a new file 'portfolio.html' with following content
    ```html
       <!DOCTYPE html>
       <html lang="en">
       <head>
         <title>Porfolio</title>
         <script type="text/javascript">
           function add(a,b){
             return a+b;
           }
         </script>
       </head>
         <body>
           <h1>Portfolio</h1>
         </body>
       </html>
    ```
8. Commit the code and push it.
9. Merge 'portfolio' branch to master and tag it as 'v0.0.2'
10. Merge 'media_integration' branch with master and tag it as 'v0.0.3'
11. Again go to 'portfolio' branch and add new file 'portfolio_item.html' and commit it. 
12. Merge 'portfolio' branch to master and tag it as 'v0.0.4'
13. Push all the changes to the remote repository
